﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace API_project_for_Christina.Constollers
{
    [ApiController]
    public class TestController : ControllerBase
    {
        [HttpGet("/test")]
        public ActionResult Test() 
        {
            return Ok("Hello Christina");
        }
    }
}
