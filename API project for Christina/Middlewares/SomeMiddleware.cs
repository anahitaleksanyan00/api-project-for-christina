﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Reflection;
using System.Threading.Tasks;

namespace API_project_for_Christina.Middlewares
{
    public class SomeMiddleware
    {
        private readonly ILogger<SomeMiddleware> _logger;
        private readonly RequestDelegate _next;
        public SomeMiddleware(RequestDelegate next, ILogger<SomeMiddleware> logger)
        {
            _logger = logger;
            _next = next;
        }
        public async Task Invoke(HttpContext context)
        {
            _logger.LogInformation("Hello from Christina");
            await _next(context);
        }
    }
}
